import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    overrides: {
      MuiCssBaseline: {
        '@global': {
          '*': {
            margin: 0,
            padding: 0,
          },
          'a': {
            textDecoration: 'none'
          },
          'li': {
            listStyleType: 'none'
          }
        },
      },
    },
  });

export default theme;