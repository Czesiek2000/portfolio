<div align="center"><h1>PORTFOLIO</h1></div>

Here you can find source code of my personal website. Live verion [here](https://czesiek2000.gitlab.io/portfolio).

This is a Next.js project bootstrapped with create-next-app.

## Technology
Technology used in this project:
* React
* Nextjs
* Material-UI

Developed with Visual Studio Code

## Development
```bash
npm install # install dependencies
npm run dev # run development server
```

Then open [localhost:3000](http://localhost:3000) to view development version of this project.

## Deployment
To deploy only thing you need to do is add all files and push them with:
```bash
git add .
git push
```

## Licence
This project is under MIT Licence.

If you :heart: or :thumbsup: this repository don't forget to leave :star: here on the repository. You are more than welcome to contribute to this project.