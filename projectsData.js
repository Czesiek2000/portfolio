const projects = [{
        title: 'FiveM Server',
        image: "",
        description: 'My first project uploaded to gitlab repository',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/fivemserver',
        details: "/server",
        categories: ['server', 'fivem']
    },
    {
        title: 'C++ template',
        image: "/cpptemplate.png",
        description: 'Boilerplate for c++ gtav scripting, for easier start with c++ mods',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/gtav_template',
        details: "/cpptemplate",
        categories: ['cpp', 'gtav']
    },
    {
        title: 'Color chat',
        image: "/color_chat.png",
        description: 'Colored chat for RAGEMP servers',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/ragemp_colored_chat',
        details: "/color_chat",
        categories: ['ragemp', 'js']
    },
    {
        title: 'Gui weapon',
        image: "/weapons.png",
        description: 'My first resource for AltV, that simplify weapon spawning',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/gui_weapon',
        details: "/gui_weapons",
        categories: ['altv', 'cef', 'js']
    },
    {
        title: 'Truck mission',
        image: "",
        description: 'My implementation of truck mission inspired by GTA Online',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/truck_mission',
        details: "/truck_mission",
        categories: ['js', 'ragemp', 'mission']
    },
    {
        title: 'Interior preview',
        image: "/interior.png",
        description: 'Second AltV project. This simplifies previewing online interiors',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/interiors',
        details: "/interior_preview",
        categories: ['js', 'altv', 'cef']
    },
    {
        title: 'VSCode autocomplete',
        image: "",
        description: 'RAGEMP syntax hint extension for VSCode. Not finished',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/ragemp-commands-hint-for-vscode',
        details: "/autocomplete",
        categories: ['ragemp', 'vscode', 'snippets']
    },
    {
        title: 'Bash snippets',
        image: "",
        description: 'Snippets for Bash scripting language for Vscode.',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/bash-snippets',
        details: "/bash",
        categories: ['bash', 'vscode', 'snippets']
    },
    {
        title: 'Commands ui',
        image: "/commands.png",
        description: 'Only ui for diplaying available commands on server',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/commands',
        details: "/commands",
        categories: ['cef']
    },
    {
        title: 'Teleport',
        image: "/teleport_to_loc.png",
        description: 'Teleport to location on the map, but without passing coords',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/teleport-to-location',
        details: "/teleport",
        categories: ['ragemp', 'js']
    },
    {
        title: 'Dev Menu',
        image: "/dev_menu.png",
        description: 'Menu that allow you to easy manipulate game',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/developer_menu',
        details: "/devmenu",
        categories: ['ragemp', 'js']
    },
    {
        title: 'Notification',
        image: "/notifications.png",
        description: 'Web based notifications using JS classes ',
        live: 'https://czesiek2000.gitlab.io/notifications/',
        code: 'https://gitlab.com/Czesiek2000/notifications',
        details: "/notification",
        categories: ['ragemp', 'cef', 'js']
    },
    {
        title: 'Scoreboard',
        image: "/scoreboard.png",
        description: 'My implementation of scoreboard. Inspired by FiveM Community',
        live: 'https://czesiek2000.gitlab.io/scoreboard/',
        code: 'https://gitlab.com/Czesiek2000/scoreboard',
        details: "/scoreboard",
        categories: ['ragemp', 'cef']
    },
    {
        title: 'Loading screen',
        image: "/loading_screen.png",
        description: 'Loading screen for FiveM. Simulates original GTAV loading screen',
        live: 'https://czesiek2000.gitlab.io/gtao-loadingscreen/',
        code: 'https://gitlab.com/Czesiek2000/gtao-loadingscreen',
        details: "/loading",
        categories: ['fivem', 'cef']
    },
    {
        title: 'UI ban plate',
        image: "/ban_plate.png",
        description: 'Web implementation of native ban / welcome gta online page',
        live: 'https://czesiek2000.gitlab.io/gtao_ban_plate',
        code: 'https://gitlab.com/Czesiek2000/gtao_ban_plate',
        details: "/ban_plate",
        categories: ['ragemp', 'cef']
    },
    {
        title: 'Map Blips',
        image: "/blips_preview.png",
        description: 'Map blips for RAGEMP',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/ragemp_blips',
        details: "/blips",
        categories: ['ragemp', 'js']
    },
    {
        title: 'RAGEMP tutorial',
        image: "/ragemp_tutorial.png",
        description: 'Tutorial for RAGEMP newbies',
        live: 'https://czesiek2000.gitlab.io/ragemp-tutorial/',
        code: 'https://gitlab.com/Czesiek2000/ragemp-tutorial',
        details: "/tutorial",
        categories: ['ragemp', 'js', 'tutorial']
    },
    {
        title: 'Gui peds',
        image: "/gui_peds.png",
        description: 'Change game peds with gui',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/gui_peds',
        details: "/gui_peds",
        categories: ['ragemp', 'cef', 'js']
    },
    {
        title: 'More projects',
        image: '/comming.png',
        description: 'More projects comming...',
        live: '',
        code: 'https://gitlab.com/Czesiek2000/',
        details: '/comming',
        categories: []
    }


]

export default projects;