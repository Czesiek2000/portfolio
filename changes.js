let data = [
    {
        date: '2021-01-17',
        added: ["Init project", "Configure project and add material ui", "Add navbar", "Add hero image", "Add about section", "Configure project for Gitlab Pages"],
        removed: []
    },
    {
        date: '2021-01-18',
        added: ["Add global theme resets", "Change navbar logo to to link to homepage", "Add custom theme"],
        removed: ["Default theme config"],
    },
    {
        date: '2021-01-19',
        added: ["Create footer component", "Add assets prefix", "Add footer component", "Add assets prefix to project card image", "Clean projects components links"],
        removed: ["Move footer code to footer component", "Clean index.js styles"],
    },
    {
        date: '2021-01-21',
        added: ["Add component to get project info from gitlab api"],
        removed: []
    },
    {
        date: '2021-02-16',
        added: ["Add tooltip for social links"],
        removed: ["Remove unnecessary import"],
    },
    {
        date: '2021-03-08',
        added: ["Update about page", "Add FiveM server starter page description", "Add cpp template page", "Update README.md file", "Add logo component", "Add ragemp color chat page"],
        removed: ["Remove unnessecary imports", "Fix commit error"],
    },
    {
        date: '2021-03-09',
        added: ["Add altv gui weapons project page"],
        removed: [],
    },
    {
        date: '2021-03-11',
        added: ["Add autocomplete page", "Add truck mission page"],
        removed: [],
    },
    {
        date: '2021-03-15',
        added: ["Add interior preview page"],
        removed: [],
    },
    {
        date: '2021-03-16',
        added: ["Add comming soon page", "Add bash extension page"],
        removed: [],
    },
    {
        date: '2021-03-21',
        added: ["Add notification page", "Add commands ui page"],
        removed: [],
    },
    {
        date: '2021-03-22',
        added: ["Add teleport command page", "Add display teleport locations", "Add available locations list for teleport command"],
        removed: ["Change imports"],
    },
    {
        date: '2021-03-24',
        added: ["Add developer menu page", "Add features list"],
        removed: ["Change imports"],
    },
    {
        date: '2021-03-29',
        added: ["Add scoreboard page"],
        removed: [],
    },
    {
        date: '2021-03-30',
        added: ["Add footer to component to scoreboard page", "Comment lines"],
        removed: ["Change hero component to logo component", "Change order of import lines"],
    },
    {
        date: '2021-04-02',
        added: ["Add loading screen overview page"],
        removed: [],
    },
    {
        date: '2021-04-06',
        added: ["Add plate page overview page"],
        removed: [],
    },
    {
        date: '2021-04-09',
        added: [],
        removed: ["Change ban plate image to fit overview page"],
    },
    {
        date: '2021-04-14',
        added: ["Add blips overview page"],
        removed: [],
    },
    {
        date: '2021-04-19',
        added: ["Add tutorial page", "Add gui peds page", "Add comming soon image"],
        removed: ["Update projects data config"],
    },
]

export default data;