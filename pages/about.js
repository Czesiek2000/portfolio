import Head from "next/head";
import Link from "next/link";

import Hero from "../components/Hero";
import Navbar from "../components/Navbar";
import Footer  from "../components/Footer";
import { Container, makeStyles } from "@material-ui/core";

import { assetPrefix } from "../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
    {
        name: 'Changelog',
        path: `${assetPrefix}/changelog`
    }
];

const useStyles = makeStyles((theme) => ({
    paragraph: {
        marginTop: '10px',
    }
}))

export default function About() {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>About page</title>
                <meta property="og:title" content="My project website" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Website that shows my work for RAGEMP" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar links={links}></Navbar>
            <Hero url="/hero.png" bigText="About page"></Hero>
            <Container fixed style={{ marginTop: '90px' }}>
                <p style={{ marginTop: '40px' }}><span style={{ fontSize: '30px'}}><strong>Hi</strong></span>, my name is <span style={{ fontSize: '18px', fontWeight: 'bold' }}>Mike</span>, you can know me better as <span style={{ fontSize: '18px', fontWeight: 'bold' }}>Czesiek2000</span>. Currently I am studying IT at the university. I am polish developer who mainly focus on web development and interfaces. My main programming language is <span style={{ fontSize: '15px', fontWeight: 'bold' }}>Javascript(JS)</span>. I can say that I am self thought developer who mainly focus on web development, but I also learn some new stuff in collage.</p>
                <p className={classes.paragraph}>I have started learning programming in 2017 which is about <span style={{ fontSize: '20px' }}><strong>{new Date().getFullYear() - 2017}</strong></span> years ago. I've started with basics of HTML and CSS then I get familiar with Javascript that is required to make scripts for websites. Then I start learning how to use Javascript frameworks mostly React and Vuejs. For GTAV development I also learned something about Nuxt and Nextjs (which I use to build this webiste).</p>
                <p className={classes.paragraph}>In college I have learned <span style={{ fontSize: '15px', fontWeight: 'bold' }}>Java</span>, basics of <span style={{ fontSize: '15px', fontWeight: 'bold' }}>C++</span>, <span style={{ fontSize: '15px', fontWeight: 'bold' }}>SQL</span> and programming extensions of SQL like <span style={{ fontSize: '15px', fontWeight: 'bold' }}>TSQL</span> from Microsoft or <span style={{ fontSize: '15px', fontWeight: 'bold' }}>PL/SQL</span> by Oracle,  <span style={{ fontSize: '15px', fontWeight: 'bold' }}>Bash</span> and Linux basics with console commands. Also I am familiar with <span style={{ fontSize: '15px', fontWeight: 'bold' }}>C# asp.net core</span> and building WebAPI with <span style={{ fontSize: '15px', fontWeight: 'bold' }}>.net core</span> and <span style={{ fontSize: '15px', fontWeight: 'bold' }}>entity framework core</span>.</p>
                <p className={classes.paragraph}>In my free time I like to play some video games, watch movies or tv series, besides programming.</p>
                <p className={classes.paragraph}>This project is made as something like portfolio that shows my work for RAGEMP, my contribution and reaserch connect with GTAV modding world, but my real portfolio can be found <Link href="https://Czesiek2000.github.io/portfolioWebsite"><a target="_blank">here</a></Link>.</p>
                <p className={classes.paragraph}>I started my journey with RAGEMP somewhere in <span style={{ fontSize: '20px'}}><strong>2018</strong></span> which is about <span style={{ fontSize: '20px'}}><strong>{new Date().getFullYear() - 2018}</strong></span>  years ago. From that time I've learned something about RAGEMP and GTAV API, which includes creating resources</p>
                <p className={classes.paragraph}>Somewhere near half of <span style={{ fontSize: '20px'}}><strong>2020</strong></span>, I've started working on researching GTAV natives (basing on the ragemp API) to try to document and help finish RAGEMP client-side API, which give me some knowledge and I created tutorial for RAGEMP newbies which can be found <Link href="https://gitlab.io/ragemp-tutorial"><a target="_blank">here</a></Link>, which still I am working on.</p>
                <p className={classes.paragraph}>Near starting of <span style={{ fontSize: '20px'}}><strong>2021</strong></span>, I've started looking at decompiled scripts to extend my natives reaserch.</p>
                <p className={classes.paragraph}>On this website I also plan to share with the world about stage of projects, what is done, what needs to be done and plans for future. I plan to make something like changelog for my projects.</p>

            </Container>

            <Footer bottom={false}></Footer>
        </div>
    )

}
