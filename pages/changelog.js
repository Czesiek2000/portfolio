import Head from 'next/head';
import Link from 'next/link';

import React from 'react';

import Navbar from '../components/Navbar';

import {  makeStyles, Container, List, ListItemText, Accordion, AccordionSummary, AccordionDetails, Typography, ListItem, ListItemIcon, Badge } from '@material-ui/core';

import { green, red } from '@material-ui/core/colors';

import { Add, ExpandMore, Remove } from '@material-ui/icons';

import { assetPrefix } from "../next.config";

import data from '../changes'

const useStyles = makeStyles((theme) => ({
    root: {
      width: '70%',
    },

    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },

    subHeading: {
        marginBottom: '40px',
    },

    toolbar: {
        height: theme.mixins.toolbar.minHeight + 80,
    },

    badge: {
        marginLeft: '44px',
        
        "& > span": {
            height: '36px',
            width: "74px",
        }
    }

}));

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
]

const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

const countCommits = () => {
    let counter = 0;
    data.forEach(d => {
        counter += d.added.length + d.removed.length
    })

    return counter;
}

export default function Changelog() {
    const classes = useStyles();

    return (
        <div>
            <Head>
                <title>Changelog page</title>
                <meta property="og:title" content="Website changelog" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Changelog page that shows logs from website creating" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            
            <Container fluid="true">
            <div className={classes.toolbar}></div>
            <Typography variant="h3" style={{ marginBottom: '40px' }}>Changelog <Badge color="secondary" badgeContent={`Total: ${countCommits()}`} className={classes.badge}></Badge></Typography>
            <p className={classes.subHeading}>Here you can see changes made in this project. More info can be found on <Link href="https://gitlab.com/Czesiek2000/portfolio">gitlab</Link></p>        
            {
                    data.map((el, index) => (
                    <Accordion key={index}>
                        <AccordionSummary expandIcon={<ExpandMore />}>
                                <Typography className={classes.heading} >{months[new Date(el.date).getMonth()]} {new Date(el.date).getDate()}, {new Date(el.date).getFullYear()} | { el.added.length + el.removed.length } changes</Typography>
                        </AccordionSummary>
                        <AccordionDetails>
                            <List>
                            {
                                el.added.map((text, index) => (
                                    <ListItem button key={ index }>
                                        <ListItemIcon>
                                            <Add style = {{ color: green[500] }}/>
                                        </ListItemIcon>
                                        <ListItemText primary={text} />
                                    </ListItem>
                                ))
                            }
                            {
                                el.removed.map((r, index) => (
                                    <ListItem button key={ index }>
                                        <ListItemIcon>
                                            <Remove style={{ color: red[500] }} />
                                        </ListItemIcon>
                                        <ListItemText primary={r} />
                                    </ListItem>
                                ))
                            }
                            </List>
                        </AccordionDetails>
                    </Accordion>
                   ))
            }
            <Accordion disabled>
                <AccordionSummary expandIcon={<ExpandMore />} id="panel2a-header">
                    <Typography className={classes.heading}>Future commits</Typography>
                </AccordionSummary>
                <AccordionDetails>
                <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                    sit amet blandit leo lobortis eget.
                </Typography>
                </AccordionDetails>
            </Accordion>
            </Container>
        </div>
    )
}