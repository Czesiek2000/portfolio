import React from 'react';

// Material ui
import { CssBaseline, Container } from '@material-ui/core';

// Components
import Navbar from '../components/Navbar';
import Hero from '../components/Hero';
import Footer from '../components/Footer';
import HeadData from "../components/HeadData";

import About from '../components/Home/About';
import Project from '../components/Home/Project';
import Languages from '../components/Home/Languages'
import Contact from '../components/Home/Contact.js'


// Import projects data
import { assetPrefix } from '../next.config';


export default function Home() {

  // Navbar home links
  const links = [
    {
      name: 'Home',
      path: `${assetPrefix}/`,
    },
    {
      name: 'About',
      path: `${assetPrefix}/about`,
    },
    {
      name: 'Projects',
      path: `${assetPrefix}/details`,
    },
    {
      name: 'Gitlab',
      path: "https://www.gitlab.com/Czesiek2000"
    },
  ];
 
  return (
    <React.Fragment>
    <CssBaseline />
    <div>
      <HeadData title="My projects website" ogTitle="My project website" description="Website that shows my work for RAGEMP"></HeadData>
      
      <Navbar links={links}></Navbar>
      <Hero url="/hero.png" bigText="Welcome to my website" smallText="I present you my work"></Hero>
      
      <Container fixed style={{ marginTop: '20px', width: "90%" }}>
        <About />
        <Project />
        <Languages />
        <Contact />
      </Container>

      <Footer></Footer>


    </div>
    </React.Fragment>
  )
}