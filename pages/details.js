import Head from 'next/head';

// Components
import Navbar from '../components/Navbar';
import Projects from '../components/Projects';
import Footer from '../components/Footer';

// Material ui 
import { Container, makeStyles } from '@material-ui/core'

import { assetPrefix } from "../next.config.js";
import projects from '../projectsData';

const useStyles = makeStyles({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 140,
    },
});


const details = () => {
    const classes = useStyles();
    const links = [
        {
            name: 'Home',
            path: `${assetPrefix}/`
        },
        {
            name: 'About',
            path: `${assetPrefix}/about`,
        },
        {
            name: 'Changelog',
            path: `${assetPrefix}/changelog`
        }
    ];
    return (
        <div>
            <Head>
                <title>Projects</title>
                <meta property="og:title" content="Subpage listing my projects" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Website that shows my work for RAGEMP" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar links={links}></Navbar>
            <div style={{ marginTop: "100px" }}>

                <Container>
                    <Projects projects={projects} sort={true}></Projects>
                </Container>
                <Footer bottom={false}></Footer>
            </div>

        </div>
    )
}

export default details;