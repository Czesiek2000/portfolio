import Head from "next/head";
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar"
import Footer from "../../components/Footer";
import ApiLink from '../../components/ApiLink';
import Logo from '../../components/Logo';

import { assetPrefix } from "../../next.config";

// Material ui components
import { Container, makeStyles } from "@material-ui/core";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const color_chat = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Color chat</title>
                <meta property="og:title" content="RAGEMP color chat" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="RAGEMP color chat resource" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar links={links}></Navbar>
            <Container fixed className={classes.root}>
            <Logo url="/static/color_chat.png"></Logo>

                <p style={{ marginTop: '40px' }}>This project is my first project that I've made for RAGEMP server. This is really simple project that I've made to get more familiar with commands and displaying html inside game chat</p>
                <p className={classes.margin}>The purpose of this project is to display chat commands and their parameters better way with colors.</p>
                <p className={classes.margin}>This was inspired by <strong>FiveM</strong> community where most of Roleplay server exists, and they use chat commands like <span style={{ fontSize: '18px' }}><strong>/me</strong></span> or <span style={{ fontSize: '18px' }}><strong>/do</strong></span>. On the preview image you can see other commands, but you can view every commands used on the main page in the README of this project.</p>
                <p className={classes.margin}>To view source code of this page go to <Link href="https://gitlab.com/Czesiek2000/ragemp_colored_chat">gitlab</Link> repository page.</p>
                
                <ApiLink id="13682889"></ApiLink>

            </Container>
            <Footer bottom={false}></Footer>
        </div>
    )
}

export default color_chat;