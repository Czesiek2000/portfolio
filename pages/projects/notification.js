import Link from "next/link";
import Head from 'next/head';

// Material ui 
import { Container, makeStyles } from '@material-ui/core';

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Logo from '../../components/Logo';
import Footer from "../../components/Footer";

// Asset prefix
import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const notification = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Notification</title>
                <meta property="og:title" content="Web notification" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Web notification made with Javascript" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            
            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
                <Logo url="/static/card_img.png"></Logo>

                <p style={{ marginTop: '40px' }}><strong>This project is not finished entirely, have some bugs and requires to rewrite or fix some functions</strong></p>
                <p className={classes.margin}>Web based project, it was made using Object Oriented Programming using Javascript. It display notifications on the web browser, but it is also has API to use inside <strong>RAGEMP</strong> server.</p>
                <p className={classes.margin}>In the repository on <strong>master</strong> branch, there is implementation for server. But if you want source code of the live page you need to checkout <strong>pages</strong> branch.</p>
                <p className={classes.margin}>If you want to see how it works or instalation manual you can checkout <Link href="https://gitlab.com/Czesiek2000/notification">gitlab</Link> repository of this project. If you want to see live verison of this project go <Link href="https://czesiek2000.gitlab.io/notifications/">here</Link>.</p>

                <ApiLink id="22086630"></ApiLink>
            </Container>
            <Footer bottom={true}></Footer>
        </div>
    )
}

export default notification;