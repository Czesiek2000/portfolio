import Link from 'next/link';
import Head from 'next/head';

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Footer from '../../components/Footer';
import Logo from '../../components/Logo';

// Material ui components
import { Container, makeStyles, Grid, Typography } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const features = [
    "Add armor, hp to player",
    "Enable super jump, god mode",
    "Termal vision",
    "Spawn car",
    "Spawn custom car",
    "Repair, lock, explode, flip, freeze, remove vehicle",
    "Manage vehicle doors",
    "Put player into vehicle or change seats",
    "Weapon with explosive ammo",
    "Explosive mele",
    "Change time to specific",
    "Add, subtract hour",
    "Freeze, unfreeze time",
    "Set time to specific day time",
    "Change weather",
    "Set infinite, refill, remove ammo",
    "Teleport to specific location",
    "Change player skin",
    "Change plate text and color",
    "Show simple speedometer with nice ui",
]

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px'
    },

    paragraph: {
        marginTop: '10px',
    },

    header: {
        marginTop: '20px',
        marginBottom: '20px',
        fontWeight: 'bold',
    },
    
    gridItem: {
        marginTop: '10px',
        fontSize: '18px',
    }

}))

const devmenu = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Dev menu</title>
                <meta property="og:title" content="Dev menu" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Menu that can easy manipulate game world" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            
            <Navbar links={links}></Navbar>
            
            <Container fixed  style={{ marginTop: '110px' }}>
                <Logo url="/static/card_img.png"></Logo>
                <p style={{ marginTop: '40px', fontWeight: 'bold', fontSize: '20px' }}>This project is not finished and still in development.</p>
                <p className={classes.paragraph}>This is simple menu that allows you to modify or change game world. It uses RAGEMP <Link href="https://rage.mp/files/file/41-nativeui/">NativeUI</Link> implementation by <Link href="https://rage.mp/profile/18996-gamingmaster/">GamingMaster</Link>.</p>
                <p className={classes.paragraph}>Creating this menu was inspired by Vespura's VMenu for Fivem. This was also made for testing some functions, or research how they works inside game. It is splitted into categories.</p>
                <p className={classes.paragraph}>Available categories: Player, Vehicle, Weapons, Time, Weather.</p>
                <p className={classes.paragraph}>If you want to checkout full source code of this project go to <Link href="https://gitlab.com/Czesiek2000/developer_menu">gitlab</Link> repository.</p>
                <Typography className={classes.header} variant="h5">Some available features:</Typography>
                <Grid container>
                    {
                        features.map((feature, index) => (
                            <Grid item xs={12} sm={6} md={6} lg={4} key={index} className={classes.gridItem}>
                                { feature }
                            </Grid>
                        ))
                    }
                </Grid>
                <ApiLink id="21393857"></ApiLink>
            
            </Container>

            <Footer bottom={false}></Footer>
        </div>
    )
}

export default devmenu;