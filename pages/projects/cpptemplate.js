import Link from 'next/link';
import Head from "next/head";

// Components
import Navbar from "../../components/Navbar"
import Hero from "../../components/Hero"
import Footer from "../../components/Footer";
import ApiLink from '../../components/ApiLink';

import { assetPrefix } from "../../next.config";

// Material ui components
import { Container, makeStyles } from "@material-ui/core";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const cpptemplate = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Cpp template</title>
                <meta property="og:title" content="Template starter in cpp" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Cpp starter for gtav modding" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            <Container fixed className={classes.root}>

            <Hero url="/cpptemplate.png"></Hero>
           
                <p style={{ marginTop: '40px' }}>When I started working with <strong>GTAV</strong> I tried to create mods or code inside singleplayer. But that requires <strong>C++</strong>, which then (and also now) for me is really hard to understand how to write something with <strong>C++</strong> using <strong>ScriptHookV</strong>ScriptHookV.</p>
                <p className={classes.margin}>I found some boilerplate for <strong>C#</strong> on the <Link href="https://gta5-mods.com">gta5mods</Link> website, so I thought, why not make some boilerplate for <strong>C++</strong>, because nothing of that type existed on the internet in that time.</p>
                <p className={classes.margin}>That was the idea of this project, to create simple starter boilerplate for C++ developers. </p>
                <p className={classes.margin}>I've never post this project on <Link href="https://gta5-mods.com">gta5mods</Link> website, so it is only available here on my gitlab repository.</p>
                <p className={classes.margin}>To view source code of this page go to <Link href="https://gitlab.com/Czesiek2000/gtav_template">gitlab</Link> repo of this project.</p>
                
                <ApiLink id="8782884"></ApiLink>

            </Container>
            <Footer></Footer>
        </div>
    )
}

export default cpptemplate;