import Head from "next/head";
import Link from "next/link";

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Footer from "../../components/Footer";
import Logo from "../../components/Logo";

// Material ui
import { Container, makeStyles } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const blips = () => {

    const classes = useStyles();

    return (
        <div>
            <Head>
                <title>Blips</title>
                <meta property="og:title" content="" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
                <Logo url="/static/blips_preview.png"></Logo>

                <p style={{ marginTop: '40px' }}>This project adds lots of blips to RAGEMP server with easy configuration and all blips are separated in collection arrays in diffrent files. Setup and installation is really easy, requires only copy paste and edit two files into your server folder.</p>
                <p className={classes.margin}>I separated files that stores blips info into separated files and locate it inside <strong>locations</strong> folder. They are just simple arrays but converted into nodejs modules. All these arrays are being merged inside <strong>data.js</strong> file. So if you want to remove some blips colleciton you need to delete it's name from <strong>data.js</strong> file. If you want to add location collection do the same as files inside <strong>location</strong> directory, create file and inside it export array of objects with your new blips value.</p>
                <p className={classes.margin}>There are also implemented labels displayed on atms in this project. To change label text see <strong>atmLabels.js</strong> file, but with these configuration you need to display only one text for all atms. For some example how to customize labels see <strong>index.js</strong> where you have some helpfull commented code.</p>
                <p className={classes.margin}>If you want to see preview images, instalation manual, how it works or source code go to gitlab <Link href="https://gitlab.com/Czesiek2000/ragemp_blips">repository</Link>.</p>

                <ApiLink id="13018849"></ApiLink>

            </Container>

            <Footer bottom={false}> </Footer>
        </div>
    )
}

export default blips;