import Head from "next/head";
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar"
import Footer from "../../components/Footer";
import ApiLink from '../../components/ApiLink';
import Logo from '../../components/Logo';

import { assetPrefix } from "../../next.config";

// Material ui components
import { Container, makeStyles } from "@material-ui/core";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const interior_preview = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Interior preview</title>
                <meta property="og:title" content="Preview of GTAV interiors" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="AltV gui preview of interiors" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
        
            <Navbar links={links}></Navbar>
            <Container fixed className={classes.root}>
            <Logo url="/static/card_img.png"></Logo>

                <p style={{ marginTop: '40px' }}>My second project that I've for <strong>AltV Multiplayer</strong>. It simplyfies loading online interiors into altv server, it uses <strong>CEF</strong>(chromium embeded framework) to display interior list. After click each interior on list, interior is loaded, then you will be teleported to interior.</p>
                <p className={classes.margin}>Idea of this project is to look see how to load interior inside <strong>AltV Multiplayer</strong>, and teleporting inside server.</p>
                <p className={classes.margin}>This project was inspired by <Link href="https://rage.mp/profile/14467-n0minal/">n0minal</Link> project from <Link href="https://rage.mp">ragemp</Link> forum.</p>
                <p className={classes.margin}>To view source code of this page go to <Link href="https://gitlab.com/Czesiek2000/ragemp-commands-hint-for-vscode">gitlab</Link> repository.</p>
                
                <ApiLink id="16015718"></ApiLink>

            </Container>
            <Footer bottom={true}></Footer>
        </div>
    )
}

export default interior_preview;