import Head from "next/head";
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar"
import Hero from "../../components/Hero"
import Footer from "../../components/Footer";
import ApiLink from '../../components/ApiLink';
import Logo from '../../components/Logo';


// Material ui components
import { Container, makeStyles } from "@material-ui/core";

// Asset prefix
import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const gui_weapon = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Gui weapons</title>
                <meta property="og:title" content="Show weapon gui" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="AltV first resource, spawn weapons with gui" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            <Container fixed style={{ marginTop: '64px' }}>
            <Logo url="/static/weapons.png"></Logo>

                <p style={{ marginTop: '40px' }}>This is my first project for <strong>AltV Multiplayer</strong>. It simplyfies spawning weapons for player with GUI which can be done with <strong>CEF(chromium embeded framework)</strong></p>
                <p className={classes.margin}>You open this gui inside game by clicking keyboard key and choose weapon you want to get. Then you will get weapon that you clicked.</p>
                <p className={classes.margin}>This project was made to get familiar with <strong>AltV Multiplayer</strong> and to compare it with <strong>RAGE Multiplayer</strong> or <strong>FiveM</strong>, where I've made few projects.</p>
                <p className={classes.margin}>Preview of design you can view here, if you want you can view more images inside gitlab repo readme to see how it looks inside game.</p>
                <p className={classes.margin}>To view source code of this page go to <Link href="https://gitlab.com/Czesiek2000/gui_weapon">gitlab</Link> repository page.</p>
                
                <ApiLink id="15967653"></ApiLink>

            </Container>
            <Footer bottom={true}></Footer>
        </div>
    )
}

export default gui_weapon;