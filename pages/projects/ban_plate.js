import Head from "next/head";
import Link from "next/link";

// Components
import Navbar from "../../components/Navbar";
import Logo from "../../components/Logo";
import Footer from "../../components/Footer";
import ApiLink from "../../components/ApiLink";

// Material ui
import { Container, makeStyles } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const ban_plate = () => {

    const classes = useStyles();

    return (
        <div>
            <Head>
                <title>Ban plate</title>
                <meta property="og:title" content="Ban plate" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Web implementation of gtav ban plate" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
                <Logo url="/static/ban_plate.png"></Logo>

                <p style={{ marginTop: '40px' }}>Ban plate is project that implements GTA Online ban plate in CEF inside game. It uses GTA font called <strong>Pricedown</strong>. This resource has some bugs that will be fixed in the future, but if you want to use it inside your server, you can easily fix it.</p>
                <p className={classes.margin}>I this repository on branch <strong>warning</strong> I implemented native ban plate. It can appear with or without background. One thing that I couldn't figure out is how to close this native warning plate, so to close it you need to restart your server or implement by yourself closing method.</p>
                <p className={classes.margin}>If you want to see preview images, instalation manual, how it works or source code go to gitlab <Link href="https://gitlab.com/Czesiek2000/gtao_ban_plate">repository</Link> or if you want to see live preview before adding it to your server checkout <Link href="https://czesiek2000.gitlab.io/gtao_ban_plate">preview</Link>.</p>
                
                <ApiLink id="20718856"></ApiLink>
                
            </Container>

            <Footer bottom={true}></Footer>
        </div>
    )
}

export default ban_plate;