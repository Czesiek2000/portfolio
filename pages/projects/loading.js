import Head from 'next/head';
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Logo from "../../components/Logo";
import Footer from "../../components/Footer";

// Material ui
import { Container, makeStyles } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const loading = () => {
    
    const classes = useStyles();

    return (
        <div>
            <Head>
                <title>Loading screen</title>
                <meta property="og:title" content="Loading screen" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Fivem loading screen inspired by GTA" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            
            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
                <Logo url="/static/card_img.png"></Logo>
                <p style={{ marginTop: '40px' }}><strong>This project has some bugs, some of them are being fixed. Some portion of code needs to be rewriten. But it is possible to run it on the server.</strong>.</p>
                <p className={classes.margin}>This project is my implementation of GTA storymode loading screen for FiveM as loading screen. It was made to learn how to create loading screen for FiveM.</p>
                <p className={classes.margin}>It has really easy configuration, to change something you need to edit <strong>config.js</strong> file, with your custom values.</p>
                <p className={classes.margin}>If you want to see preview images, how it works or instalation manual you can checkout <Link href="https://gitlab.com/Czesiek2000/gtao-loadingscreen">gitlab</Link> repository of this project. If you want to see live verison of this project see web preview <Link href="https://czesiek2000.gitlab.io/gtao-loadingscreen">here</Link>. There is also open <Link href="https://forum.cfx.re/t/release-gta-online-loading-screen/635597">thread</Link> on fivem forum, which you might also want to look at.</p>
                <ApiLink id="13158435"></ApiLink>
            
            </Container>

            <Footer bottom={true}></Footer>
        </div>
    )
}

export default loading;