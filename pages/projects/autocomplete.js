import Head from "next/head";
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar"
import Hero from "../../components/Hero"
import Footer from "../../components/Footer";
import ApiLink from '../../components/ApiLink';
import Logo from '../../components/Logo';

import { assetPrefix } from "../../next.config";

// Material ui components
import { Container, makeStyles } from "@material-ui/core";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const autocomplete = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Autocomplete</title>
                <meta property="og:title" content="ragemp autocomplete" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="RAGEMP autocomplete for Visual Studio Code" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            <Container fixed className={classes.root}>
            <Logo url="/static/card_img.png"></Logo>

                <p style={{ marginTop: '40px' }}><strong>NOTE: This project is in work in progress state. </strong></p>
                <p className={classes.margin}>If you making resources for RAGEMP with Visual Studio Code you can use this project. It just bring all data from wiki to snippet and after click <strong>CTRL+SPACE</strong> will display hint window that.</p>
                <p className={classes.margin}>After selecting snippet that you want to use in your code hit <strong>TAB</strong> or <strong>ENTER</strong>. This will bring wiki code example to your editor.</p>
                <p className={classes.margin}>Data for this project is being collected, so that why it is in progress of creating.</p>
                <p className={classes.margin}>To view source code of this page go to <Link href="https://gitlab.com/Czesiek2000/ragemp-commands-hint-for-vscode">gitlab</Link></p>
                
                <ApiLink id="12329139"></ApiLink>

            </Container>
            <Footer bottom={true}></Footer>
        </div>
    )
}

export default autocomplete;