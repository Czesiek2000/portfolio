import Head from "next/head";
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar"
import Footer from "../../components/Footer";
import ApiLink from '../../components/ApiLink';
import Logo from '../../components/Logo';

import { assetPrefix } from "../../next.config";

// Material ui components
import { Container, makeStyles } from "@material-ui/core";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const truck_mission = () => {
    const classes = useStyles();
    return (
        <div>
             <Head>
                <title>Truck mission</title>
                <meta property="og:title" content="Truck delivery mission" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="RAGEMP truck mission inspired by GTAOnline" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar links={links}></Navbar>
            <Container fixed className={classes.root}>
            <Logo url="/static/card_img.png"></Logo>

                <p style={{ marginTop: '40px' }}><strong>Note: This is not finished, it works when you run it on the server but it has lot of spaghetti code that needs to be rewrite.</strong> </p>
                <p className={classes.margin}>This project is <strong>RAGE Multiplayer</strong> implementation of GTAV mod / GTA Online mission.</p>
                <p className={classes.margin}>Basic idea of this project is it spawns truck and trailer in random places on the map. Then it create route from your place to spawned truck. If you enter your truck that was spawned route to your trailer will be created.</p>
                <p className={classes.margin}>If you have trailer and truck you can go to your destination point, which is marked on your map. After that your truck and trailer will be despawn.</p>
                <p className={classes.margin}>To view source code of this page go to <Link href="https://gitlab.com/Czesiek2000/ragemp-commands-hint-for-vscode">gitlab</Link> repository of this project.</p>
                
                <ApiLink id="20718427"></ApiLink>

            </Container>
            <Footer></Footer>
        </div>
    )
}

export default truck_mission;