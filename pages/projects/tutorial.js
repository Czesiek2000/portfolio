import Head  from "next/head";
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Footer from "../../components/Footer";
import Logo from "../../components/Logo";

// Material ui
import { Container, makeStyles } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',
        
        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));


const tutorial = () => {
    
    const classes = useStyles();
    
    return (
        <div>
            <Head>
                <title>RAGEMP tutorial</title>
                <meta property="og:title" content="RAGE Multiplayer tutorial" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Tutorial for new RAGE Multiplayer developers" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>

            <Container fixed className={classes.root}>
                <Logo url="/static/ragemp_tutorial.png"></Logo>
                <p style={{ marginTop: '40px' }}><strong>This project is still in progress of building, some content are also being updated and added</strong>.</p>
                <p className={classes.margin}>RAGEMP tutorial is made for new developers that comes to RAGE Muliplayer. It was made to help them start their journey with building resources and start their own servers.</p>
                <p className={classes.margin}>It shows basic usage of RAGEMP API but also how to use built in CEF, basics of Javascript and Nodejs programming, SQL and databases and how to use them in RAGEMP, and many more RAGE Multiplayer features.</p>
                <p className={classes.margin}>If you want to see preview images, instalation manual, how it works or source code go to gitlab <Link href="https://gitlab.com/Czesiek2000/ragemp-tutorial">repository</Link>. This tutorial is available on the web, to read it click <Link href="https://czesiek2000.gitlab.io/ragemp-tutorial">here</Link>.</p>

                <ApiLink id="21142619" full={ true }></ApiLink>

            </Container>

            <Footer bottom={true}></Footer>
        </div>
    )
}

export default tutorial;