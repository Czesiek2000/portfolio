import Link from 'next/link';
import Head from 'next/head';

// Components
import Navbar from '../../components/Navbar';
import Hero from '../../components/Hero';
import Footer from '../../components/Footer';

import { assetPrefix } from "../../next.config";

// Material UI components
import { Container, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const links = [ 
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    }
]

const server = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>FiveM server</title>
                <meta property="og:title" content="server boilerplate" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Fivem server boilerplate" />
                <link rel="icon" href="/favicon.ico" />
            </Head>


            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
            
            <Hero url="/card_img.png" bigText="FiveM server" smallText="Starter fivem server"></Hero>

                <p style={{ marginTop: '40px' }}>When I started developing for alternative <strong>GTAV</strong> servers, I started with <strong>FiveM</strong>. For that reason I created simple this simple resource that was be some kind of backup or starter for <strong>FiveM</strong> new developers. </p>
                <p className={classes.margin}>This is starter boilerplate for fiveM server. It was created as backup of server when I first started using alternative <strong>GTAV</strong> servers.</p>
                <p className={classes.margin}><strong>NOTE: This repository wasn't update since it's published date on gitlab. It may be not up to date. If you want to start your own server go to <Link href="https://forum.cfx.re/">FiveM</Link> website for newer version</strong></p>
                <p className={classes.margin}>To view source code of this page go to <Link href="https://gitlab.com/Czesiek2000/fivemserver">gitlab</Link></p>
            
            </Container>
            
            <Footer bottom={false}></Footer>
        </div>
    )
}

export default server;