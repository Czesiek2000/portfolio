import Head  from "next/head";
import Link from "next/link";

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Logo from "../../components/Logo";
import Footer from "../../components/Footer";

// Material ui
import { Container, makeStyles } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',
        
        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const gui_peds = () => {

    const classes = useStyles();

    return (
        <div>
            <Head>
                <title>Gui peds</title>
                <meta property="og:title" content="Ingame gui display of peds" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="GUI interface to change ped skin inside game" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>

            <Container fixed className={classes.root}>
                <Logo url="/static/gui_peds.png"></Logo>

                <p style={{ marginTop: '40px' }}>GUI peds is simple CEF app that helps players change ped skin with CEF window. After clicking <strong>F5</strong> on your keyboard new CEF window will show up. It displays all peds that are available in GTAV in one scrollable window (I'm trying to update peds list after new update is released).</p>
                <p className={classes.margin}>Player need to click ped skin that want to change character skin to selected. Then click <strong>X</strong> button that displays in top left corner of the window to close peds window.</p>
                <p className={classes.margin}>Instalation is also really simple, it requires copying files from <strong>client_packages</strong> <strong>packages</strong> folders and copy to the appropriate folders inside <strong>server-files</strong> folder.</p>
                <p className={classes.margin}>If you want to see preview images, instalation manual, how it works or source code go to gitlab <Link href="https://gitlab.com/Czesiek2000/gui_peds">repository</Link>.</p>
                <ApiLink id="14419578"></ApiLink>

            </Container>

            <Footer bottom={true}></Footer>
        </div>
    )
}

export default gui_peds;