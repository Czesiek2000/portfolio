import Head  from "next/head";

import Navbar from "../../components/Navbar";

import { assetPrefix } from "../../next.config";

import { makeStyles } from "@material-ui/core";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles({
    heroImage: {
        position: 'relative',
        height: '94vh',
        backgroundAttachment: 'fixed',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',

    },

    heroText: {
        fontWeight: 'bold',
        textAlign: 'center',
        position: 'absolute', 
        fontSize: '24px', 
        top: '170px', 
        left: '18%',
        color: 'white',
        textShadow: '3px 9px 8px #000000',

        "@media (min-width: 375px)": {
            top: '200px',
            left: '23%',
            fontSize: '26px',    
        },

        "@media (min-width: 768px)": {
            top: '200px',
            left: '30%',
            fontSize: '41px',    
        },

        "@media (min-width: 1024px)": {
            top: '190px',
            left: '30%',
            fontSize: '55px',    
        },
        
        "@media (min-width: 1440px)": {
            top: '190px',
            left: '35%',
            fontSize: '74px',    
        },
    },

    smallText: {
        fontSize: '18px',
        textAlign: 'center',

        "@media (min-width: 1440px)": {
            fontSize: '30px',    
        },
    },

    line: {
        marginTop: '20px',
        height: '3px',
        border: 'none',
        backgroundColor: '#f5f5f5',
        width: '138px',
        marginLeft: 'auto',
        marginRight: 'auto',
    }
})

const comming = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Comming soon</title>
                <meta property="og:title" content="New projects comming" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Comming soon website for new projects" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            <div style={{ marginTop: '56px', overflowY: 'hidden' }}>
                <div className={classes.heroImage} style={{ backgroundImage: `url("${assetPrefix}/static/comming.png")`, width: '100%' }}>
                    <div className={classes.heroText}>
                        <p style={{ textTransform: 'uppercase' }}>Comming soon</p>
                        <p className={classes.smallText}>More project are comming</p>
                        <hr className={classes.line}/> 
                    </div>
                </div>
            </div>
        </div>
    )
}

export default comming;