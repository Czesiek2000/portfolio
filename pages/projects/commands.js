import Head from 'next/head';

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Logo from "../../components/Logo";
import Footer from "../../components/Footer";

// Material ui
import { Container, Link, makeStyles } from "@material-ui/core";

// Prefix
import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        height: "633px",
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    paragraph: {
        marginTop: '10px',
    }
}));

const commands = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Commands</title>
                <meta property="og:title" content="UI commands" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="UI description of server commands" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            
            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
                <Logo url="/static/card_img.png"></Logo>

                <p style={{ marginTop: '40px' }}>This is web based design to display commands on your server. It was projected to run inside <strong>RAGEMP</strong> server, but it is <strong>not finished</strong> and doesn't have implementation for RAGEMP server or other alternative servers</p>
                <p className={classes.paragraph}>Idea behind this project was to create resource that can be added to server by server developers and easily display their commands, but I haven't finished it. I was planned to create command or button to display commands page with button click or with command.</p>
                <p className={classes.paragraph}>If you want to use this resource in your server you need to create command by yourself or handle button click to display this window.</p>
                <p className={classes.paragraph}>To see full source code of this project checkout gitlab <Link href="https://gitlab.com/Czesiek2000/commands" target="_blank">repository</Link> site.</p>
                <ApiLink id="20717956"></ApiLink>
            </Container>
            <Footer></Footer>
        </div>
    )
}

export default commands;