import Head from 'next/head'
import Link from 'next/link';

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Logo from "../../components/Logo";
import Footer from '../../components/Footer';

// Material ui
import { Container, makeStyles } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));

const scoreboard = () => {

    const classes = useStyles();

    return (
        <div>
            <Head>
                <title>Scoreboard</title>
                <meta property="og:title" content="Game scoreboard" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Customizable players scoreboard" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
                <Logo url="/static/scoreboard.png"></Logo>
                
                <p style={{ marginTop: '40px' }}>This project shows scoreboard for players. It was first made and inspired from FiveM, but finally was made inside RAGEMP.</p>
                <p className={classes.margin}>UI was made inside in game CEF. Most of functionality is handled by the browser. Server send global value of max players from server config file, current playing players and player id, player name, player socialclub id, current player armour, current player health.</p>
                <p className={classes.margin}>To show scoreboard click <strong>z</strong> key. Then browser will toggle this scoreboard window.</p>
                <p className={classes.margin}>You can test this scoreboard in your browser. If you go to preview page you can test this scoreboard with functions that are made specially for this preview. To execute it open browser console with <strong>F12</strong> and type functions that are displayed on screen.</p>
                <p className={classes.margin}>If you want to see how it works or instalation manual you can checkout <Link href="https://gitlab.com/Czesiek2000/scoreboard">gitlab</Link> repository of this project. If you want to see live verison of this project see web preview <Link href="https://czesiek2000.gitlab.io/scoreboard/">here</Link>.</p>
                
                <ApiLink id="22496554"></ApiLink>

            </Container>

            <Footer></Footer>
        </div>
    )
}

export default scoreboard;