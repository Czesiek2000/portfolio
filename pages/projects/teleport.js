import Head from "next/head";

// Components
import Navbar from "../../components/Navbar";
import ApiLink from "../../components/ApiLink";
import Footer from "../../components/Footer";
import Logo from "../../components/Logo";

import { Container, Grid, makeStyles, Typography } from "@material-ui/core";

import { assetPrefix } from "../../next.config";

import locations from '../../locations'

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        height: '1480px',
        marginTop: '64px',        
    },

    paragraph: {
        marginTop: '10px',
    },

    header: {
        marginTop: '40px',
        fontWeight: 'bold',
    },
    
    gridItem: {
        marginTop: '10px',
        fontSize: '20px',
    }
}));

const teleport = () => {
    const classes = useStyles();

    return (
        <div>
            <Head>
                <title>Teleport commands</title>
                <meta property="og:title" content="Teleport commands" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Chat command to teleport to diffrent location without giving coords" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            
            <Navbar links={links}></Navbar>
            
            <Container fixed className={classes.root}>
                <Logo url="/static/card_img.png"></Logo>
                <p style={{ marginTop: '40px'}}>This project is simple command that teleports you to specific coordinates, but you don't need to pass coordinates just type name of place, that is specified in config file, where you want to teleport</p>
                <p className={classes.paragraph}>After you hit enter it will show black screen for second or two and it the background it will teleport you to that location. After teleport is finished black screen will disapear and your character will appear on screen.</p>
                <p className={classes.paragraph}>This resource has out of the box some popular places stored, and you can easy add new places.</p>

                <Typography className={classes.header} variant="h5">Available locations: </Typography>
                <Container fixed style={{ marginLeft: '70px', marginTop: '15px' }}>
                    <Grid container justify="center">
                        {
                            locations.map((location, i) => (
                                <Grid item xs={12} sm={6} md={4} lg={4} className={classes.gridItem} key={i}>
                                    { location }
                                </Grid>
                            ))
                        }
                    </Grid>
                </Container>
                
                <ApiLink id="21207049"></ApiLink>

            </Container>

            <Footer bottom={false}></Footer>
        </div>
    )
}

export default teleport;