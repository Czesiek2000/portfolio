import Head from 'next/head';
import Link from "next/link";

// Material ui
import { Container, makeStyles } from "@material-ui/core";

// Custom components
import Footer from '../../components/Footer';
import Logo  from "../../components/Logo";
import ApiLink from '../../components/ApiLink';
import Navbar from '../../components/Navbar';

// Asset prefix to fix links
import { assetPrefix } from "../../next.config";

const links = [
    {
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles(() => ({
    root: {
        marginTop: '64px',

        '@media (max-width: 375px)': {
            marginBottom: "-223px"
        },
    },

    margin: {
        marginTop: '10px',
    }
}));


const bash = () => {
    const classes = useStyles();
    return (
        <div>
            <Head>
                <title>Bash snippets</title>
                <meta property="og:title" content="Snippets for bash language" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="Bash snippets for visual studio code" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <Navbar links={links}></Navbar>
            <Container fixed className={classes.root}>
                <Logo url="/static/card_img.png"></Logo>

                <p style={{ marginTop: '40px' }}>This project is made as helper for <strong>Visual Studio Code</strong> for <strong>Bash</strong> scripting language. It is displaying helpers snippets for typing words.</p>
                <p className={classes.margin}>To use this extension, go to project <Link href="https://gitlab.com/Czesiek2000/bash-snippets">repository</Link> and see how to install it on your computer. Then go to Visual Studio Code, create <strong>bash</strong> file with <strong>.sh</strong> extension, then open it in vscode and start typing some keywords, then you should see hint popup, click one of the displayed hint and inside your file will be added some part of starting template code.</p>
                <p className={classes.margin}>Idea for this project was that it will help writing bash scripts, by helping with or hinting bash syntax. </p>
                <p className={classes.margin}>If you want to see how it works or instalation manual you can checkout <Link href="https://gitlab.com/Czesiek2000/bash-snippets">gitlab</Link> repository of this project.</p>

                <ApiLink id="21016347"></ApiLink>
            </Container>
            <Footer bottom={true}></Footer>
        </div>
    )
}

export default bash;