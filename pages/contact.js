import Head from 'next/head';

import React, { useState } from 'react';

import { Button, Container, makeStyles, MenuItem, TextField, Typography, } from "@material-ui/core";

import Navbar from "../components/Navbar";

import { assetPrefix } from '../next.config'

const links=[{
        name: 'Home',
        path: `${assetPrefix}/`
    },
    {
        name: 'About',
        path: `${assetPrefix}/about`
    },
    {
        name: 'Projects',
        path: `${assetPrefix}/details`
    },
];

const useStyles = makeStyles((theme) => ({
    root: {
        width: '40%',
    },

    toolbar: {
        height: theme.mixins.toolbar.minHeight + 140,
    },

    text: {
        marginBottom: '40px',
    },
    
    input: {
        // width: '60%',
        margin: theme.spacing(2),
    },

    button: {
        display: 'block',
        marginTop: '15px',
        marginRight: 'auto',
        marginLeft: 'auto',
    }
}));

const topics = [
    {
        key: 1,
        value: 'About projects',
        label: 'About projects',
    },
    {
        key: 2,
        value: 'Collaboration',
        label: 'Collaboration',
    },
    {
        key: 3,
        value: 'Project suggestion',
        label: 'Project suggestion',
    },
    {
        key: 4,
        value: 'Other',
        label: 'Other',
    },
];

export default function contact() {
    const classes = useStyles();
    const [topic, setTopic] = useState(topics[0].value);
    const [submit, setSubmit] = useState(false);
    const [name, setName] = useState("");
    const [nick, setNick] = useState("");
    const [mail, setMail] = useState("");
    const [message, setMessage] = useState("");
    const [atopic, setATopic] = useState("");
    
    const handleChange = (event) => {
        setTopic(event.target.value);
    };

    const handleSubmit = event => {
        setSubmit(true);
        event.preventDefault();
        send(name, nick, mail, topic, message);
        if (topic === 'Other') {
            send(name, nick, mail, atopic, message);
        }
    };

    const send = (nameValue, nickname, email, topics, msg) => {
        console.log(`submitted: ${nameValue}, ${nickname}, ${email}, ${topics}, ${msg}`);
    }

    return (
        <div>
            <Head>
                <title>Contact me</title>
                <meta property="og:title" content="contact" />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content="On this page you can easily contact with me" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <Navbar links={links}></Navbar>
            <div className={classes.toolbar}></div>
            {submit || <Container fixed className={classes.root}>
                <Typography variant="h3" className={classes.text}>Contact with me</Typography>
                <form onSubmit={handleSubmit}>
                    <TextField id="name" label="Name" autoComplete="none" fullWidth className={classes.input} onChange={(e) => setName(e.target.value)} required/>
                    <TextField id="nick" label="Nick" autoComplete="none" fullWidth className={classes.input} onChange={(e) => setNick(e.target.value)}/>
                    <TextField id="reply" label="Replay mail" autoComplete="none" fullWidth className={classes.input} onChange={(e) => setMail(e.target.value)} required/>
                    {topic === "Other" || 
                        <TextField id="standard-select-currency" select label="Select" value={topic} onChange={handleChange} helperText="Please select topic of your message" className={classes.input}>
                            {topics.map((option) => (
                                <MenuItem key={option.value} value={option.value}>
                                    {option.label}
                                </MenuItem>
                            ))}
                        </TextField>
                    }
                    {topic === "Other" && <TextField id="atopic" label="Your topic" fullWidth className={classes.input} onChange={(e) => setATopic(e.target.value)}></TextField>}
                    <TextField id="message" label="Message" multiline rows={5} autoComplete="none" fullWidth className={classes.input} onChange={(e) => setMessage(e.target.value)} required/>
                    <Button type="submit" variant="contained" color="primary" className={classes.button}>Submit</Button>
                </form>
            </Container>}
            {submit && <div>
                <Container fixed>
                    <Typography variant="h3">Thanks for contacting with me</Typography>
                    <p>Name: {name}</p>
                    <p>Nick: {nick}</p>
                    <p>Replay mail: {mail}</p>
                    {topic !== "Other" && <p>Topic: {topic}</p>}
                    {topic === "Other" && <p>Alternative topic {atopic}</p> }
                    <p>Message: {message}</p>
                </Container>
            </div>}
        </div>
    )
}