import React, { useEffect, useState } from "react";
import { PropTypes } from "prop-types";

import {
	Card,
	CardActionArea,
	CardActions,
	CardContent,
	CardMedia,
	Button,
	Grid,
	Typography,
	makeStyles,
	Chip,
} from "@material-ui/core";

import { assetPrefix } from "../next.config";

const useStyles = makeStyles({
	root: {
		maxWidth: "345px",
		height: "350px",
		display: "flex",
		flexDirection: "column",
	},

	media: {
		height: 140,
	},

	chips: {
		marginBottom: "20px",
		"&>div": {
			marginLeft: "10px",
			cursor: "pointer",
		},
	},

	chip: {
		marginLeft: "5px",
		marginTop: "2px",
	},

	header: {
		marginBottom: "15px",
	},
});

const Projects = ({ projects, limit, sort }) => {
	const classes = useStyles();
	const [category, setCategory] = useState("all");
	const [sortedProjects, setProjects] = useState([]);

	const categories = [
		"all",
		...Array.from(
			new Set(
				[].concat.apply(
					[],
					projects.map((project) => project.categories)
				)
			)
		),
	];

	useEffect(() => {
		let project =
			category === "all"
				? projects
				: projects.filter((project) => project.categories.includes(category));
		setProjects(project);
	}, [category]);

	return (
		<div style={{ paddingTop: "20px " }}>
			{!sort ? (
				<Grid
					container
					spacing={3}
					direction="row"
					justify="center"
					alignItems="flex-start">
					{projects.slice(0, limit).map((project, index) => (
						<Grid item xs={12} sm={6} md={4} lg={3} key={index}>
							<Card className={classes.root}>
								<CardMedia
									className={classes.media}
									image={
										project.image.length !== 0
											? `${assetPrefix}/static${project.image}`
											: `${assetPrefix}/static/card_img.png`
									}
									title="Project title"
									style={{ backgroundPosition: "inherit" }}
								/>
								<CardContent>
									<Typography gutterBottom variant="h5" component="h2">
										{project.title}
									</Typography>
									<Typography
										variant="body2"
										color="textSecondary"
										component="p"
										style={{ paddingBottom: "5px" }}>
										{project.description}
									</Typography>
								</CardContent>
								<CardActions
									style={{ marginTop: "auto", marginBottom: "16px" }}>
									<Button
										size="small"
										variant="contained"
										color="primary"
										disabled={!project.live}>
										<a
											href={project.live}
											target="_blank"
											style={{
												color: !project.live ? "rba(0,0,0, 0.65)" : "#fff",
											}}>
											Live
										</a>
									</Button>

									<Button
										size="small"
										variant="contained"
										color="primary"
										disabled={project.code.length === 0}>
										<a
											href={project.code}
											target="_blank"
											style={{
												color: !project.code ? "rba(0,0,0, 0.65)" : "#fff",
											}}>
											Code
										</a>
									</Button>

									<Button
										size="small"
										variant="contained"
										color="primary"
										disabled={project.details.length == 0}>
										<a
											href={
												project.details.length !== 0
													? `${assetPrefix}/projects${project.details}`
													: "https://gitlab.com/Czesiek2000"
											}
											target="_blank"
											style={{
												color: !project.details ? "rba(0,0,0, 0.65)" : "#fff",
											}}>
											Details
										</a>
									</Button>
								</CardActions>
							</Card>
						</Grid>
					))}
				</Grid>
			) : (
				<div>
					<Typography variant="h4" className={classes.header}>
						Total projects: {sortedProjects.length}
					</Typography>
					<div className={classes.chips}>
						{categories.sort().map((c, index) => (
							<Chip
								label={c}
								color={category === c ? "secondary" : "default"}
								key={index}
								className={classes.chip}
								onClick={() => setCategory(c)}
							/>
						))}
					</div>
					<Grid
						container
						spacing={3}
						direction="row"
						justify="center"
						alignItems="flex-start">
						{sortedProjects.map((project, index) => (
							<Grid item xs={12} sm={6} md={4} lg={3} key={index}>
								<Card className={classes.root}>
									<CardActionArea>
										<CardMedia
											className={classes.media}
											image={
												project.image.length !== 0
													? `${assetPrefix}/static${project.image}`
													: `${assetPrefix}/static/card_img.png`
											}
											title="Project title"
											style={{ backgroundPosition: "inherit" }}
										/>
										<CardContent>
											<Typography gutterBottom variant="h5" component="h2">
												{project.title}
											</Typography>
											<Typography
												variant="body2"
												color="textSecondary"
												component="p">
												{project.description}
											</Typography>
											<div style={{ marginTop: "10px" }}>
												{project.categories.map((cat, index) => (
													<Chip
														label={cat}
														key={index}
														className={classes.chip}
														color={cat === category ? "secondary" : "default"}
														onClick={() => setCategory(cat)}
													/>
												))}
											</div>
										</CardContent>
									</CardActionArea>
									<CardActions
										style={{ paddingBottom: "16px", marginTop: "auto" }}>
										<Button
											size="small"
											variant="contained"
											color="primary"
											disabled={!project.live}>
											<a
												href={project.live}
												target="_blank"
												style={{
													color: !project.live ? "rba(0,0,0, 0.65)" : "#fff",
												}}>
												Live
											</a>
										</Button>

										<Button
											size="small"
											variant="contained"
											color="primary"
											disabled={project.code.length === 0}>
											<a
												href={project.code}
												target="_blank"
												style={{
													color:
														project.code.length === 0
															? "rba(0,0,0, 0.65)"
															: "#fff",
												}}>
												Code
											</a>
										</Button>

										<Button
											size="small"
											variant="contained"
											color="primary"
											disabled={project.details.length == 0}>
											<a
												href={
													project.details.length !== 0
														? `${assetPrefix}/projects${project.details}`
														: "https://gitlab.com/Czesiek2000"
												}
												target="_blank"
												style={{
													color:
														project.details.length === 0
															? "rba(0,0,0, 0.65)"
															: "#fff",
												}}>
												Details
											</a>
										</Button>
									</CardActions>
								</Card>
							</Grid>
						))}
					</Grid>
				</div>
			)}
		</div>
	);
};
export default Projects;

Projects.propTypes = {
	projects: PropTypes.array,
	limit: PropTypes.number,
	sort: PropTypes.bool,
};
