import { assetPrefix } from "../next.config";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(() => ({
    image: {
        height: '300px',
        backgroundSize: 'auto',
        // backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat',
        // marginTop: '70px',
        width: '90%',
        margin: '0 auto',

        "@media screen and (min-width: 1024px)": {
            marginLeft: '140px',   
        },
        
        "@media screen and (min-width: 1440px)": {
            marginLeft: '260px',   
        },


    },
}))

const Logo = (props) => {
    const classes = useStyles();
    return (
        <div>
            <div style={{ backgroundImage: `url(${assetPrefix}${props.url})` }} className={ classes.image }></div>
        </div>
    )
}

export default Logo;