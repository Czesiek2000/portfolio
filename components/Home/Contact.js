
import { Typography, Grid, Tooltip, Fab, makeStyles } from "@material-ui/core"

// Icons
import CodeIcon from '@material-ui/icons/Code';
import ForumIcon from '@material-ui/icons/Forum';
import GitHubIcon from '@material-ui/icons/GitHub';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles((theme) => ({
    link: {
        marginRight: '10px',
        color: '#fff',
    },
    
    margin: {
        margin: theme.spacing(3),
        padding: '20px',
    },

    extendedIcon: {
        margin: theme.spacing(1),
    },
  
}))

export default function Contact() {
    const classes = useStyles();

    return (
        <div style={{ height: '280px', marginTop: '40px' }}>
            <Typography variant="h4" >Where to find me?</Typography>
            <p style={{ marginTop: '10px' }}>You can find me in many places in the internet.</p>
            <p style={{ marginTop: '10px' }}>You can checkout my Gitlab account where I upload my work and research connected with GTAV and RAGEMP development.</p>
            <p style={{ marginTop: '10px' }}>I also have personal profile on Github, where you can find more web development stuff. </p>
            <p style={{ marginTop: '10px' }}>To contact with me use given links, which are RAGEMP forum and Discord.</p>
            <Grid container style={{ marginTop: '20px' }} justify="center" >
                <Grid item>
                <Tooltip title="Gitlab link" placement="top" arrow>
                    <Fab variant="extended" size="small" style={{ backgroundColor: "#ff9800", color: "#fff" }} aria-label="add" className={classes.margin}>
                    <CodeIcon className={classes.extendedIcon} />
                        <a href="https://gitlab.com/Czesiek2000" className={classes.link} target="_blank">Gitlab</a>
                    </Fab>
                </Tooltip>
                </Grid>
                <Grid item>
                <Tooltip title="Github link" placement="top" arrow>
                    <Fab variant="extended" size="small" style={{ backgroundColor: "#78909c" }} aria-label="add" className={classes.margin}>
                    <GitHubIcon className={classes.extendedIcon} />
                        <a href="https://github.com/Czesiek2000" className={classes.link} target="_blank">Github</a>
                    </Fab>
                </Tooltip>
                </Grid>
                <Grid item>
                <Tooltip title="RAGEMP forum" placement="top" arrow>
                    <Fab variant="extended" size="small" style={{ backgroundColor: "#fdd835" }} aria-label="add" className={classes.margin}>
                    <ForumIcon className={classes.extendedIcon} style={{ color: '#212121'}}/>
                        <a href="https://rage.mp/profile/70051-czesiek/" className={classes.link} style={{ color: "#000" }} target="_blank">RAGEMP forum</a>
                    </Fab>
                </Tooltip>
                </Grid>
                <Grid item>
                <Tooltip title="Discord link" placement="top" arrow>
                    <Fab variant="extended" size="small" style={{ backgroundColor: "#3949ab", color: "#fff" }} aria-label="add" className={classes.margin}>
                    <EditIcon className={classes.extendedIcon}/>
                        <a href="https://discordapp.com/users/206875427631923200" className={classes.link} target="_blank">Discord</a>
                    </Fab>
                </Tooltip>
                </Grid>
            </Grid>
        </div>
    )
}