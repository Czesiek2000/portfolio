import Link from 'next/link';

import { Typography } from "@material-ui/core";

export default function About() {
    
    function age() {
        const birthday = new Date('04-08-2000');
        let ageDifMs = Date.now() - birthday.getTime();
        let ageDate = new Date(ageDifMs);
        return Math.abs(ageDate.getUTCFullYear() - 1970);
    }
    
    return (
        <div>
            <Typography variant="h4">About</Typography>
            <p style={{ marginTop: '10px', lineHeight: '25px' }}>Hi, my name is Mike, you can recognize me as Czesiek2000. Currently I am <span style={{ fontWeight: "bold" }}>{ age() }</span> years old. 
            Here you can see my work that I upload to my <Link href="https://gitlab.com/Czesiek2000" target="_blank">Gitlab</Link> repository, which stores all my projects made for GTAV alternative servers like RAGEMP(mainly), AltV, FiveM.
            I started working on alternative servers in 2018. I create mainly user interfaces, because mainly I am frontend developer, but I also create stuff like menus with NativeUI or some missions. More about me and this project <Link href="/about">here</Link>.</p>
        </div>
    )
}