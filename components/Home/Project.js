
import { Typography, Button, makeStyles } from "@material-ui/core";

import Projects from '../Projects';

import { assetPrefix } from '../../next.config';

import projects from '../../projectsData';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: '20px auto',
        display: 'flex',
        alignItems: 'center',
        width: '20%',
    },
    toolbar: {
        height: theme.mixins.toolbar.minHeight + 50,
    }
}))
export default function Project() {
    const limit = 8;
    const classes = useStyles();

    return (
        <div style={{ marginTop: '25px' }}>
            <Typography variant="h4" className="project-header">Projects</Typography>
            <Projects projects={projects} limit={limit} sort={false}></Projects>
            <Button variant="contained" color="primary" size="large" className={classes.button} href={`${assetPrefix}/details`}>
                Show more
            </Button>
        </div>
    )
}