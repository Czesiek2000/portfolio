
import { Typography, Grid, Paper, makeStyles, Tooltip } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
        margin: theme.spacing(2)
    },

    text: {
        textAlign: 'center',
        marginTop: '10px',
    }
}));

// const mainLanguages = ['HTML', 'CSS', 'JS', 'Typescript(still learning)', 'Bash', 'JAVA', 'C++(Little bit)', 'Python(Little bit)', 'C#/asp.net']
const mainLanguages = [{ lang: 'HTML', info: 'Main language used to create interfaces' }, { lang: 'CSS', info: 'Main styling language when creating interfaces' }, { lang: 'Typescript', info: 'Know little bit, still learnig' }, { lang: 'Bash', info: 'Used when dealing with local files'}, { lang: 'Java', info: 'Main language learned in University'}, { lang: 'C++', info: 'Learned little bit at university'}, { lang: 'Python', info: 'Learned little bit, used mainly for web scraping'}, { lang: 'C#/asp.net', info: 'Also learned at university along with blazor and how to make apis'}]
const tools = ['Visual Studio Code', 'Adobe XD', 'Figma', 'RAGEMP API', 'Terminal', 'Git', 'npm']
const frameworks = ['Vuejs (Still learning)', 'React (Still learning)', 'Vuepress', 'Next.js', 'Gatsby', 'Nuxt.js']
const databases = ['SQL', 'TSQL', 'PL/SQL', 'Firebase', 'SQLite']

export default function Languages() {
    const classes = useStyles();
    return (
        <div>
            <Typography variant="h4">Languages</Typography>
            <p style={{ marginTop: "20px", marginBottom: "20px" }}>This is list of programming skills. I'm still learning these things, but it doesn't mean that I can't write some app in listed languages or in other technology based on this.</p>
            <Grid container >
            <Grid item xs={12} sm={6} md={3}>
                <Typography variant="h5" className={classes.text}>Main languages</Typography>
                
                    {
                        mainLanguages.map((language, index) => (
                            <Tooltip title={language.info} key={index}>
                                <Paper className={classes.paper} >{language.lang}</Paper>
                            </Tooltip>
                        ))
                    }

            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <Typography variant="h5" className={classes.text}>Tools</Typography>
                
                    {tools.map((tool, index) => (
                        <Paper className={classes.paper} key={index}>{ tool }</Paper>

                    ))}

            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <Typography variant="h5" className={classes.text}>Frameworks</Typography>
                    {
                        frameworks.map((framework, index) => (
                            <Paper className={classes.paper} key={index}>{ framework }</Paper>

                        ))    
                    }

            </Grid>
            <Grid item xs={12} sm={6} md={3}>
                <Typography variant="h5" className={classes.text}>Databases</Typography>
                    
                    {
                        databases.map((db, index) => (
                            <Paper className={classes.paper} key={index}>{ db }</Paper>

                        ))
                    }
            </Grid>
            </Grid>
        </div>
    )
}