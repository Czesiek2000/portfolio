import Link from 'next/link';
import React, { useState, useEffect } from 'react';
import { AppBar, Toolbar, Typography, Button, IconButton, makeStyles, Icon, Drawer, MenuItem } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

import { assetPrefix } from "../next.config";

const useStyles = makeStyles((theme) => ({
    header: {
        backgroundColor: "#400CCC",
        paddingRight: "79px",
        paddingLeft: "118px",
        "@media (max-width: 900px)": {
          paddingLeft: 0,
        },
        justifyContent: 'space-between'
      },
      logo: {
        fontFamily: "Roboto, sans-serif",
        fontWeight: 500,
        color: "#FFFEFE",
        textAlign: "right",
        "@media (max-width: 900px)": {
            paddingLeft: 0,
            display: 'none',
          },
      },
      menuButton: {
        fontFamily: "Open Sans, sans-serif",
        fontWeight: 400,
        size: "18px",
        marginLeft: "18px",
        marginRight: "10px",
      },
      toolbar: {
        display: "flex",
        justifyContent: "space-between",
      },
      drawerContainer: {
        padding: "20px 30px",
      },

  }));
  

const Navbar = ( props ) => {
    const classes = useStyles();
    const [state, setState] = useState({
        drawerOpen: false,
        mobileView: false,
    })

    const { drawerOpen, mobileView } = state;
    useEffect(() => {
        const setResponsiveness = () => {
          return window.innerWidth < 900
            ? setState((prevState) => ({ ...prevState, mobileView: true }))
            : setState((prevState) => ({ ...prevState, mobileView: false }));
        };
        setResponsiveness();
        window.addEventListener("resize", () => setResponsiveness());
    }, []);

    const displayDesktop = () => {
        return (
          <Toolbar className={classes.toolbar}>
            {logo}
            <div>{getMenuButtons()}</div>
          </Toolbar>
        );
      };

      const displayMobile = () => {
        const handleDrawerOpen = () => setState((prevState) => ({ ...prevState, drawerOpen: true }));
        const handleDrawerClose = () => setState((prevState) => ({ ...prevState, drawerOpen: false }));
    
        return (
          <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" aria-haspopup="true" onClick={handleDrawerOpen}>
              <MenuIcon />
            </IconButton>
            <Drawer anchor="left" open={drawerOpen} onClose={handleDrawerClose}>
              <div className={classes.drawerContainer}>{getDrawerChoices()}</div>
            </Drawer>
    
            <div>{logo}</div>
          </Toolbar>
        );
      };
    
      const getDrawerChoices = () => {
        return props.links.map(({ name, path }) => {
          return (
            <Link href={path} color="inherit" style={{ textDecoration: "none" }} key={name}>
              <MenuItem>{name}</MenuItem>
            </Link>
          );
        });
      };
    
      const logo = (
        <Typography variant="h6" component="h1" className={classes.logo}>
          <Link href={`${assetPrefix}/`}><a style={{ color: "#fff" }}>Czesiek2000</a></Link>
        </Typography>
      );
    
      const getMenuButtons = () => {
        return props.links.map(({ name, path }) => {
          return (
            <Button color="inherit" href={path} className={classes.menuButton} key={name}>
              {name}
            </Button>
          );
        });
      };
    
      return (
        <header>
          <AppBar className={classes.header}>
            {mobileView ? displayMobile() : displayDesktop()}
          </AppBar>
        </header>
      );
    }


export default Navbar;