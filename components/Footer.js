import { Grid, makeStyles } from "@material-ui/core";
import FavoriteIcon from '@material-ui/icons/Favorite';

import { useEffect, useRef } from "react";

const useStyles = makeStyles((theme) => ({
  footer: {
    backgroundColor: "#303f9f", 
    padding: '15px',
    // paddingTop: '24px',
    height: '190px',
    fontSize: '18px',
    marginTop: '370px',
    
    '& > div > div': {
      paddingTop: '19px',
    },
    
    "@media (min-width: 375px)": {
      marginTop: '270px',
    },
    
    "@media (min-width: 425px)": {
      marginTop: '185px',
      paddingTop: '26px'
    },
    
    "@media (min-width: 768px)": {
      marginTop: '100px',
      '& > div': {
        textAlign: "center",
      }
    },
    
    "@media (min-width: 1024px)": {
      marginTop: '50px',
      paddingTop: "70px"
    },
  },

  "@keyframes bounce": {
      "from": {
        // transform: "translateY(0)"
        paddingBottom: 0
      },
      "to": {
        // transform: "translateY(-15px)"
        paddingBottom: '15px'
      }
    },

  bottom: {
    // position: 'fixed',
    // bottom: 0,
    width: '100%',
    flex: 1,
  },

  icon: {
    animation: '1s $bounce alternate infinite',
  }

}));

const Footer = (props) => {
  
  const pushDown = () => {
  useEffect(() => {
      let element = footer.current;
      let height = element.clientHeight;
      console.log(element.clientHeight);
      if (typeof window !== "undefined") {
        console.log(window.screen.height);
        if (height < window.screen.height) {
          element.style.position = 'fixed';
          element.style.bottom = '0';
          element.style.width = '100%';
          element.style.marginTop = '110px';
        }
      }
    }, [])
  };

  const footer = useRef();
  const classes = useStyles();

    return (
      <div className={`${classes.footer} footer`} ref={footer}>
        { props.bottom === true ? pushDown() : '' }
          <Grid container justify="center" alignItems="center" >
              <Grid item style={{ color: "#fff", paddingLeft: '20px' }} xs={12} sm={12} md={6}>
                &copy; Czesiek2000, {new Date().getFullYear()}. All rights reservered
              </Grid>
              <Grid item style={{ color: "#fff", paddingLeft: '20px' }} xs={12} sm={12} md={6}>
                Made with <FavoriteIcon style={{ color: "#d32f2f", boxShadow: '0 8px 6px -6px black' }} className={classes.icon}></FavoriteIcon> and React, Next.js, MaterialUI
              </Grid>
          </Grid>
      </div>
    )
}

export default Footer;