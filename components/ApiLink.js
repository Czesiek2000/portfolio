import { Grid, Typography } from '@material-ui/core';
import { useState, useEffect, useRef } from 'react'

const addZero = (date) => {
    return (date < 10) ? "0" + date : date;
}

const changeDate = (date) => {
    let newDate = new Date(date).toUTCString();
    let parseDate = `${addZero(new Date(newDate).getDate())} / ${addZero(new Date(newDate).getMonth() + 1)} / ${new Date(newDate).getFullYear()} ${addZero(new Date(newDate).getHours())} : ${addZero(new Date(newDate).getMinutes())} : ${addZero(new Date(newDate).getSeconds())}`;
    return `${parseDate}`
}

const lastUpdate = (commitDate) => {
    const oneDay = 24 * 60 * 60 * 1000;
    let commit = new Date(commitDate);
    let today = new Date();
    // let dateDifference = today - new Date(commitDate).getDate();
    const dateDifference = Math.round(Math.abs((new Date(today.getFullYear(), today.getMonth(), today.getDay()) - new Date(commit.getFullYear(), commit.getMonth(), commit.getDay())) / oneDay));
    return `Last update ${dateDifference} days ago`;
}

const ApiLink = (props) => {
    const { id, full } = props;
    const API = `https://gitlab.com/api/v4/projects/${id}/repository/commits${full ? '?per_page=100' : ''}`
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [items, setItems] = useState({});
    const [commits, setCommits] = useState(0);

    useEffect(() => {
        fetch(API)
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setItems(result[0]);
                    setCommits(result.length);
                },

                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
            .finally(() => {
                setIsLoaded(false);
            });
    }, [])
    
    return (
        <div>
            {isLoaded ? <div>Loading...</div> : <div>
                <Typography style={{ marginTop: '40px' }} variant="h5">Project details: </Typography>
                <Grid container justify="center" style={{ marginTop: '20px' }}>
                    <Grid item xs={12} sm={6} md={4} lg={4} style={{ marginTop: '10px' }}>
                        <strong>Authored date: </strong> {items !== undefined ? changeDate(items.authored_date) : 'Cannot access data'}
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={4} style={{ marginTop: '10px' }}>
                        <strong>Commit id:</strong> {items !== undefined ? items.short_id : 'Cannot access data'}
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={4} style={{ marginTop: '10px' }}>
                        <strong>Commit message:</strong> {items !== undefined ? items.message : 'Cannot access data'}
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={4} style={{ marginTop: '10px' }}>
                        <strong>Commit title: </strong>{items !== undefined ? items.title || 'There is no title' : 'Cannot access data'}
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={4} style={{ marginTop: '10px' }}>
                        <strong>Total commits: </strong>{items !== undefined ? commits : 'Cannot access data'}
                    </Grid>
                    <Grid item xs={12} sm={6} md={4} lg={4} style={{ marginTop: '10px' }}>
                        <strong>Last commit: </strong>{items !== undefined ? lastUpdate(items.authored_date) : 'Cannot access data'}
                    </Grid>
                </Grid>
            </div>
            }
            { error && <div>Error: {error}</div> }
        </div>
    );
}

export default ApiLink;