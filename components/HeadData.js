import Head from 'next/head';

const HeadData = ( { title, ogTitle, description } ) => {
    return (
        <div>
            <Head>
                <title>{ title }</title>
                <meta property="og:title" content={ ogTitle } />
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <meta name="description" content={ description } />
                <meta name="author" content="Czesiek2000" />
                <meta name="keywords" content="webdev, webdevelopment, portfolio, ragemp, ragemultiplayer, gtav, grand theft auto v, gta online, altV, altV multiplayer, fivem" />
                <meta charSet="utf-8" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
        </div>
    )
}

export default HeadData;