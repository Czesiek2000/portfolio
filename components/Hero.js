import { makeStyles } from '@material-ui/core';
import { assetPrefix } from '../next.config.js';

const useStyles = makeStyles({
    heroImage: {
        position: 'relative',
        height: '400px',
        "@media (min-width: 1024px)": {
            height: '600px',
        },
        '& > div': {
            height: '100%',
        }
    },

    heroText: {
        textAlign: 'center',
        position: 'absolute', 
        fontSize: '18px', 
        top: '170px', 
        left: '23%',
        color: 'white',
        "@media (min-width: 768px)": {
            top: '200px',
            left: '36%',
            fontSize: '20px',    
        },

        "@media (min-width: 1024px)": {
            top: '190px',
            left: '40%',
            fontSize: '22px',    
        },
        
        "@media (min-width: 1440px)": {
            top: '190px',
            left: '45%',
            fontSize: '24px',    
        },
    },

    smallText: {
        fontSize: '16px',
        textAlign: 'center',
    },
})

const Hero = () => {
    const classes = useStyles();
    return(
        <div className="hero">
            <div className={classes.heroImage} style={{ backgroundImage: `url("${assetPrefix}/static/hero.png")`, width: '100%', backgroundSize: 'cover' }}>
                <div className={classes.heroText}>
                    <p>Welcome to my website </p>
                    <p className={classes.smallText}>I present you my work</p>    
                </div>
            </div>
        </div>
    )
}

export default Hero;